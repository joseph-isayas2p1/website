module Discoslab.Test.OneWord exposing
    ( OneWord
    , errorMessage
    , fromString
    , toString
    )


type OneWord
    = OneWord String


fromString : String -> Maybe OneWord
fromString w =
    let
        wordCount =
            List.length (String.words w)
    in
    if wordCount == 1 then
        Just (OneWord w)

    else
        Nothing


toString : OneWord -> String
toString (OneWord w) =
    w


errorMessage : String
errorMessage =
    "More or less than one word."
