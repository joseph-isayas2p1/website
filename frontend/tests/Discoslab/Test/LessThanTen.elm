module Discoslab.Test.LessThanTen exposing
    ( LessThanTen
    , errorMessage
    , fromInt
    , toInt
    )


type LessThanTen
    = LessThanTen Int


errorMessage : String
errorMessage =
    "Greater than or equal to ten."


fromInt : Int -> Maybe LessThanTen
fromInt n =
    if n < 10 then
        Just (LessThanTen n)

    else
        Nothing


toInt : LessThanTen -> Int
toInt (LessThanTen n) =
    n
