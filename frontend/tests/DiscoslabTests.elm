module DiscoslabTests exposing (tests)

import Discoslab.Url.BasePath as BasePath
import Expect
import Test exposing (Test)


tests : Test
tests =
    Test.describe "Discoslab"
        [ Test.test "temp test to fix ci for now until i have time to implement real tests" <|
            \() ->
                Expect.equal (BasePath.fromString "api") (BasePath.fromString "/api/")
        ]
