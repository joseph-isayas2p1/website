module Discoslab exposing
    ( Model
    , Msg(..)
    , init
    , subscriptions
    , update
    , view
    )

import Browser
import Discoslab.Effect as Effect exposing (Effect)
import Discoslab.Effect.Navigation as Navigation
import Discoslab.Ui.Layouts.Root as RootLayout
import Discoslab.Ui.Text.Heading as Heading
import Discoslab.Ui.Text.Paragraph as Paragraph
import Url exposing (Url)


type alias Model =
    {}


type Msg
    = UrlChanged Url
    | UrlRequested Browser.UrlRequest


init : Url -> ( Model, Effect Msg )
init _ =
    ( Model
    , Effect.None
    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        UrlChanged _ ->
            ( model
            , Effect.None
            )

        UrlRequested (Browser.Internal url) ->
            ( model
            , Url.toString url
                |> Navigation.PushUrl
                |> Effect.Navigation
            )

        UrlRequested (Browser.External url) ->
            ( model
            , Navigation.Load url
                |> Effect.Navigation
            )


view : Model -> Browser.Document Msg
view _ =
    { title = "Disco's Lab"
    , body =
        RootLayout.view
            [ Heading.config "Welcome to Disco's Lab"
                |> Heading.setLevel Heading.High
                |> Heading.setAlignment Heading.Center
                |> Heading.view
            , Paragraph.config "Not much to see here yet, but check back soon!"
                |> Paragraph.view
            ]
    }
