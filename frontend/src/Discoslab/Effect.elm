module Discoslab.Effect exposing
    ( Dependencies
    , Effect(..)
    , map
    , toCmd
    )

import Browser.Navigation
import Discoslab.Effect.Logging as Logging
import Discoslab.Effect.Navigation as Navigation
import Discoslab.Url.BasePath exposing (BasePath)


type alias Dependencies msg =
    { api : BasePath
    , namespace : List String
    , minSeverity : Logging.Severity
    , noOp : msg
    , navigationKey : Browser.Navigation.Key
    }


type Effect msg
    = None
    | Batch (List (Effect msg))
    | Logging Logging.Effect
    | Navigation Navigation.Effect


map : (a -> b) -> Effect a -> Effect b
map f effect =
    case effect of
        None ->
            None

        Batch effects ->
            Batch (List.map (map f) effects)

        Logging e ->
            Logging e

        Navigation e ->
            Navigation e


toCmd : Dependencies msg -> Effect msg -> Cmd msg
toCmd deps effect =
    case effect of
        None ->
            Cmd.none

        Batch batchedEffects ->
            batchedEffects
                |> List.map (toCmd deps)
                |> Cmd.batch

        Logging e ->
            Logging.toCmd deps e

        Navigation e ->
            Navigation.toCmd deps e
