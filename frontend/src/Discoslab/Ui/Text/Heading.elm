module Discoslab.Ui.Text.Heading exposing
    ( Alignment(..)
    , Config
    , Level(..)
    , config
    , setAlignment
    , setLevel
    , setSubHeader
    , view
    )

import Html exposing (Attribute, Html)
import Html.Attributes as Attrs


type Level
    = High
    | Normal
    | Low


type Alignment
    = Left
    | Center


type Config
    = Config
        { heading : String
        , level : Level
        , alignment : Alignment
        , subHeading : Maybe String
        }


config : String -> Config
config heading =
    Config
        { heading = heading
        , level = Normal
        , alignment = Left
        , subHeading = Nothing
        }


setLevel : Level -> Config -> Config
setLevel level (Config conf) =
    Config { conf | level = level }


setAlignment : Alignment -> Config -> Config
setAlignment alignment (Config conf) =
    Config { conf | alignment = alignment }


setSubHeader : String -> Config -> Config
setSubHeader subHeading (Config conf) =
    Config { conf | subHeading = Just subHeading }


h : Level -> List (Attribute msg) -> List (Html msg) -> Html msg
h level =
    case level of
        High ->
            Html.h1

        Normal ->
            Html.h2

        Low ->
            Html.h3


modifier : Level -> String
modifier level =
    case level of
        High ->
            "heading--high"

        Normal ->
            "heading--normal"

        Low ->
            "heading--low"


align : Alignment -> String
align alignment =
    case alignment of
        Left ->
            "heading--left"

        Center ->
            "heading--center"


view : Config -> Html msg
view (Config conf) =
    let
        topLevelAttrs =
            [ Attrs.class "heading"
            , Attrs.class (modifier conf.level)
            , Attrs.class (align conf.alignment)
            ]
    in
    case conf.subHeading of
        Nothing ->
            h conf.level
                (Attrs.class "heading__main" :: topLevelAttrs)
                [ Html.text conf.heading
                ]

        Just subHeading ->
            Html.div
                topLevelAttrs
                [ h conf.level [ Attrs.class "heading__main" ] [ Html.text conf.heading ]
                , Html.p [ Attrs.class "heading__sub" ] [ Html.text subHeading ]
                ]
