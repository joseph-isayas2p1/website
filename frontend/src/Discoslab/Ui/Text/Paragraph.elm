module Discoslab.Ui.Text.Paragraph exposing
    ( Config
    , config
    , view
    )

import Html exposing (Html)
import Html.Attributes as Attrs


type Config
    = Config
        { text : String
        }


config : String -> Config
config text =
    Config { text = text }


view : Config -> Html msg
view (Config conf) =
    Html.p [ Attrs.class "paragraph" ] [ Html.text conf.text ]
