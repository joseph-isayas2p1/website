module Discoslab.Ui.Text.Link exposing
    ( Config
    , config
    , setDisplayText
    , view
    )

import Html exposing (Html)
import Html.Attributes as Attrs


type Config
    = Config
        { href : String
        , displayText : Maybe String
        }


config : String -> Config
config href =
    Config
        { href = href
        , displayText = Nothing
        }


setDisplayText : String -> Config -> Config
setDisplayText displayText (Config conf) =
    Config { conf | displayText = Just displayText }


view : Config -> Html msg
view (Config conf) =
    let
        linkText =
            Maybe.withDefault conf.href conf.displayText
    in
    Html.a
        [ Attrs.class "link"
        , Attrs.href conf.href
        ]
        [ Html.text linkText ]
