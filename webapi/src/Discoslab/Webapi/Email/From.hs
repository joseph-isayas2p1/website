-- | Record and functions for dealing with the from address of an email.
module Discoslab.Webapi.Email.From
  ( EmailFrom (..)
  , vars
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (..))
import Discoslab.Webapi.Email.Address (EmailAddress)
import Discoslab.Webapi.Email.Address qualified as EmailAddress
import Discoslab.Webapi.Email.Name (EmailName)
import Discoslab.Webapi.Email.Name qualified as EmailName
import Env qualified
import GHC.Generics (Generic)


-- | Email address an email was sent from. Allows you to optional add a name, such as "John Smith".
data EmailFrom = EmailFrom
  { name :: Maybe EmailName
  , address :: EmailAddress
  }
  deriving (Show, Eq, Generic)


-- | Parse the 'FromAddress' from environment variables.
vars :: Env.Parser Env.Error EmailFrom
vars =
  Env.prefixed "FROM_" $
    EmailFrom
      <$> Env.optional EmailName.var
      <*> EmailAddress.var


instance Aeson.ToJSON EmailFrom
instance Aeson.FromJSON EmailFrom


instance OpenApi.ToSchema EmailFrom where
  declareNamedSchema _ = do
    namedSchema <-
      OpenApi.genericDeclareNamedSchema @EmailFrom
        OpenApi.defaultSchemaOptions
        Proxy

    pure $
      namedSchema
        & OpenApi.name ?~ "EmailFrom"
        & OpenApi.schema . OpenApi.description ?~ "Email address the message was sent from."
