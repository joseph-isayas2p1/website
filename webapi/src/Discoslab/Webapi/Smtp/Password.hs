-- | Type and functions for a password to a smtp server login.
module Discoslab.Webapi.Smtp.Password
  ( Password
  , Error (..)
  , errorText
  , fromText
  , toText
  , var
  )
where

import Data.Coerce (coerce)
import Data.Text (Text)
import Data.Text qualified as Text
import Discoslab.Webapi.Shared.Env (parseString)
import Env qualified


-- | Password used to login into a smtp server.
newtype Password = Password Text
  deriving (Eq)


-- | Never shows the actual password
instance Show Password where
  show _ =
    "<HIDDEN SMTP PASSWORD>"


-- | Error explaining why a 'Password' could not be constructed.
data Error
  = Empty
  deriving (Show, Eq, Ord, Enum, Bounded)


-- | Convert an 'Error' to human suitable text message.
errorText :: Error -> Text
errorText = \case
  Empty -> "SMTP password is empty."


-- | Convert a SMTP password from 'Text'.
-- See 'Error' for reasons why this may fail.
fromText :: Text -> Either Error Password
fromText text
  | Text.null text = Left Empty
  | otherwise = Right $ Password text


-- | Convety 'Password' to 'Text'.
toText :: Password -> Text
toText =
  coerce


-- | Parse 'Password' from an environment variable.
var :: Env.Parser Env.Error Password
var =
  let password =
        parseString $ \text ->
          case fromText text of
            Left _ -> Nothing
            Right value -> Just value
   in Env.sensitive . Env.var password "PASSWORD" . mconcat $
        [ Env.help . Text.unpack $
            "Password for logging into a SMTP server."
        ]
