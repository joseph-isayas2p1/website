-- | Record for a SMTP login.
module Discoslab.Webapi.Smtp.Login
  ( Login (..)
  , vars
  )
where

import Discoslab.Webapi.Smtp.Password (Password)
import Discoslab.Webapi.Smtp.Password qualified as Password
import Discoslab.Webapi.Smtp.UserName (UserName)
import Discoslab.Webapi.Smtp.UserName qualified as UserName
import Env qualified


-- | User name and password for a SMTP server.
data Login = Login
  { userName :: UserName
  , password :: Password
  }
  deriving (Show, Eq)


-- | Parse the 'Login' from environment variables.
vars :: Env.Parser Env.Error Login
vars =
  Login
    <$> UserName.var
    <*> Password.var
