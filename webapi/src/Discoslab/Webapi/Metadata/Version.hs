-- | Type and functions for describing the application's version.
module Discoslab.Webapi.Metadata.Version
  ( Version
  , fromText
  , toText
  , def
  , var
  )
where

import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Version (showVersion)
import Discoslab.Webapi.Shared.Env (parseString)
import Env qualified
import Paths_discoslab_webapi qualified as Paths


-- | Version of the application. Usually a git sha.
newtype Version = Version Text
  deriving (Show, Eq)


-- | Convert a 'Text' to 'Version'. Any non-empty string will work.
fromText :: Text -> Maybe Version
fromText text
  | Text.null strippedText = Nothing
  | otherwise = Just $ Version strippedText
 where
  strippedText = Text.strip text


-- | Convert 'Version' to 'Text'.
toText :: Version -> Text
toText =
  coerce


-- | Default 'Version'. Set to whatever the version is in the cabal file.
def :: Version
def =
  Version . Text.pack $ showVersion Paths.version


-- | Parse 'Version' from an environment variable.
var :: Env.Parser Env.Error Version
var =
  Env.var (parseString fromText) "VERSION" . mconcat $
    [ Env.help "Version of the application. Usually a git sha. Any non-empty string is valid."
    , Env.def def
    , Env.helpDef (Text.unpack . toText)
    ]


instance Aeson.ToJSON Version where
  toJSON = Aeson.toJSON . toText
  toEncoding = Aeson.toEncoding . toText


instance Aeson.FromJSON Version where
  parseJSON = Aeson.withText "Version" $ \text ->
    case fromText text of
      Nothing -> fail "Version may not be an empty string."
      Just value -> pure value
