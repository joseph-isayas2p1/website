-- | Root of the HTTP server.
module Discoslab.Webapi.Http
  ( makeWaiApplication
  , start
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad (when)
import Control.Monad.IO.Unlift (MonadUnliftIO (withRunInIO))
import Control.Monad.Reader (ask)
import Data.Function ((&))
import Data.Proxy (Proxy (Proxy))
import Discoslab.Webapi.AppM (AppM)
import Discoslab.Webapi.AppM qualified as AppM
import Discoslab.Webapi.Http.Config (HttpConfig (..))
import Discoslab.Webapi.Http.EnableSwagger (EnableSwagger)
import Discoslab.Webapi.Http.EnableSwagger qualified as EnableSwagger
import Discoslab.Webapi.Http.Middleware (unliftApplication)
import Discoslab.Webapi.Http.Middleware qualified as Middleware
import Discoslab.Webapi.Http.Routes qualified as Routes
import Discoslab.Webapi.Metadata.Version (Version)
import GHC.Stack (HasCallStack)
import Katip qualified
import Network.Wai qualified as Wai
import Network.Wai.Handler.Warp qualified as Warp
import Servant (hoistServer, serve)


-- | Construct the WAI application.
makeWaiApplication :: HasCallStack => Version -> EnableSwagger -> AppM Wai.Application
makeWaiApplication version enableSwagger = do
  let toApplication api apiServer =
        Middleware.apply $ \request send -> do
          appData <- ask

          let application =
                let hoistedServer = hoistServer api (AppM.toHandler appData) apiServer
                 in serve api hoistedServer

          withRunInIO $ \runInIO ->
            application request (runInIO . send)

  unliftApplication $
    if EnableSwagger.toBool enableSwagger
      then toApplication (Proxy @Routes.ApiWithSwagger) (Routes.serverWithSwagger version)
      else toApplication (Proxy @Routes.Api) Routes.server


-- | Start the WAI application on the provided port.
start :: HasCallStack => Version -> HttpConfig -> AppM ()
start version config =
  checkpointCallStack . Katip.katipAddNamespace "http" $ do
    application <- makeWaiApplication version config.enableSwagger

    withRunInIO $ \runInIO ->
      let beforeMainLoop = do
            let uri = "http://localhost:" <> Katip.ls (show config.port)

            runInIO . Katip.logLocM Katip.InfoS $ "Server running at " <> uri

            when (EnableSwagger.toBool config.enableSwagger) $
              runInIO . Katip.logLocM Katip.InfoS $
                "Visit Swagger at " <> uri <> "/docs"

          settings =
            Warp.defaultSettings
              & Warp.setBeforeMainLoop beforeMainLoop
              & Warp.setPort config.port
              & Warp.setOnException (\_ _ -> pure ()) -- omit logging because middleware should have done this already
       in Warp.runSettings settings application
