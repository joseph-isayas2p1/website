module Discoslab.Webapi.Alerts.Http
  ( Api
  , server
  )
where

import Discoslab.Webapi.Alerts.Http.Post qualified as Post
import Discoslab.Webapi.AppM (AppM)
import Servant (HasServer (ServerT))


type Api =
  Post.Api


server :: ServerT Api AppM
server =
  Post.server
