{-# LANGUAGE OverloadedLists #-}

module Discoslab.Webapi.Alerts.Http.Post.Request
  ( PostRequest (..)
  )
where

import Control.Lens ((.~), (?~))
import Data.Aeson (KeyValue ((.=)))
import Data.Aeson qualified as Aeson
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (Proxy))
import Data.Text (Text)
import GHC.Generics (Generic)
import Katip qualified


data PostRequest = PostRequest
  { severity :: Katip.Severity
  , message :: Text
  , context :: Maybe Aeson.Value
  , namespace :: Maybe [Text]
  }
  deriving (Show, Eq, Generic)


instance Aeson.FromJSON PostRequest


instance OpenApi.ToSchema PostRequest where
  declareNamedSchema _ = do
    objectSchema <- OpenApi.declareSchemaRef @Aeson.Object Proxy
    textList <- OpenApi.declareSchemaRef @[Text] Proxy
    textSchema <- OpenApi.declareSchemaRef @Text Proxy

    pure $
      OpenApi.NamedSchema (Just "AlertsPostRequest") $
        mempty
          & OpenApi.type_ ?~ OpenApi.OpenApiObject
          & OpenApi.description ?~ "Request shape for posting an alert."
          & OpenApi.required
            .~ [ "severity"
               , "message"
               ]
          & OpenApi.properties
            .~ [
                 ( "severity"
                 , textSchema
                    & OpenApi._Inline . OpenApi.description ?~ "Severity of the alert."
                    & OpenApi._Inline . OpenApi.enum_ ?~ fmap Aeson.toJSON [Katip.DebugS ..]
                    & OpenApi._Inline . OpenApi.example ?~ Aeson.toJSON Katip.ErrorS
                 )
               ,
                 ( "message"
                 , textSchema
                    & OpenApi._Inline . OpenApi.description ?~ "Message part of the alert."
                    & OpenApi._Inline . OpenApi.example ?~ Aeson.toJSON @Text "Something bad happened..."
                 )
               ,
                 ( "context"
                 , objectSchema
                    & OpenApi._Inline . OpenApi.description ?~ "Can be any arbitrary JSON, not just an object."
                    & OpenApi._Inline . OpenApi.example ?~ Aeson.toJSON (Aeson.object ["userId" .= (123 :: Integer), "page" .= ("contacts" :: Text)])
                 )
               ,
                 ( "namespace"
                 , textList
                    & OpenApi._Inline . OpenApi.description ?~ "Additional namespace to add to the alert."
                    & OpenApi._Inline . OpenApi.example ?~ Aeson.toJSON @[Text] []
                 )
               ]
