-- | Functions for dealing with types that implement both 'Bounded' and 'Enum'.
module Discoslab.Webapi.Shared.BoundedEnum
  ( fromText
  , parseJSON
  )
where

import Data.Aeson qualified as Aeson
import Data.Aeson.Types qualified as AesonTypes
import Data.Proxy (Proxy (Proxy))
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Typeable (Typeable, typeRep)


-- | Strip trailing/leading white space and convert to case fold so we
-- can do case-insensitive searches later.
normalizeText :: Text -> Text
normalizeText =
  Text.toCaseFold . Text.strip


-- | Convert from 'Text' based off the values that 'toText' can produce.
-- This ensures that your 'toText' and 'fromText' functions are always in sync which each other.
fromText :: forall a. (Bounded a, Enum a) => (a -> Text) -> Text -> Maybe a
fromText toText rawText =
  lookup (normalizeText rawText) $
    [ (normalizeText $ toText option, option)
    | option <- [minBound @a ..]
    ]


-- | Parse JSON based off the values that 'toText' can produce.
parseJSON :: forall a. (Bounded a, Enum a, Typeable a) => (a -> Text) -> Aeson.Value -> AesonTypes.Parser a
parseJSON toText =
  let typeName = show $ typeRep @_ @a Proxy
   in Aeson.withText typeName $ \rawText ->
        case fromText toText rawText of
          Nothing -> fail . Text.unpack $ "Invalid " <> Text.pack typeName <> " detected: " <> rawText
          Just a -> pure a
