-- | Type and functions for configuring the logging.
module Discoslab.Webapi.Logging.Config
  ( LoggingConfig (..)
  , def
  , vars
  )
where

import Discoslab.Webapi.Logging.Color (Color)
import Discoslab.Webapi.Logging.Color qualified as Color
import Discoslab.Webapi.Logging.Format (LogFormat)
import Discoslab.Webapi.Logging.Format qualified as LogFormat
import Discoslab.Webapi.Logging.MinSeverity (MinSeverity)
import Discoslab.Webapi.Logging.MinSeverity qualified as MinSeverity
import Env qualified


-- | Configuration for setting up logging.
data LoggingConfig = LoggingConfig
  { minSeverity :: MinSeverity
  , format :: LogFormat
  , color :: Color
  }
  deriving (Show, Eq)


-- | Default 'LoggingConfig'. Uses the @def@ function from the respective modules.
def :: LoggingConfig
def =
  LoggingConfig
    { minSeverity = MinSeverity.def
    , format = LogFormat.def
    , color = Color.def
    }


-- | Parse the 'LoggingConfig' from environment variables.
vars :: Env.Parser Env.Error LoggingConfig
vars =
  Env.prefixed "LOGGING_" $
    LoggingConfig
      <$> MinSeverity.var
      <*> LogFormat.var
      <*> Color.var
