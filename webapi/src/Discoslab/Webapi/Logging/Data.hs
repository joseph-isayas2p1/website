-- | Type and functions for keeping track of the required data for logging.
module Discoslab.Webapi.Logging.Data
  ( LoggingData (..)
  , noLogs
  , withStdout
  , runKatip
  , liftKatip
  , liftKatipIO
  )
where

import Control.Exception (bracket)
import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad.IO.Unlift (MonadUnliftIO (..))
import Control.Monad.Reader
import Data.Has (Has (..))
import Discoslab.Webapi.Logging.Color qualified as Color
import Discoslab.Webapi.Logging.Config (LoggingConfig)
import Discoslab.Webapi.Logging.Config qualified as LoggingConfig
import Discoslab.Webapi.Logging.Format qualified as Format
import Discoslab.Webapi.Logging.MinSeverity qualified as MinSeverity
import Discoslab.Webapi.Metadata (Metadata)
import Discoslab.Webapi.Metadata qualified as Metadata
import Discoslab.Webapi.Metadata.Environment qualified as Environment
import GHC.Stack (HasCallStack)
import Katip qualified
import System.IO (stdout)


-- | Data we need to do logging.
data LoggingData = LoggingData
  { env :: Katip.LogEnv
  , contexts :: Katip.LogContexts
  , namespace :: Katip.Namespace
  }


-- | Root namespace for this application. All logs will start with this namespace.
rootNamespace :: Katip.Namespace
rootNamespace =
  Katip.Namespace ["discoslab", "webapi"]


-- | 'LoggingData' that doesn't record any logs.
-- This is mostly useful for running automated tests.
noLogs :: MonadIO m => m LoggingData
noLogs = do
  env <- liftIO $ Katip.initLogEnv rootNamespace "nologs"
  pure $
    LoggingData
      { env
      , contexts = mempty
      , namespace = Katip.Namespace []
      }


-- | Make the log env and attach a 'stdout' scribe to it. It's important
-- that the caller takes care to clean this up appropriately, which
-- is why you should call 'withStdout' instead.
makeEnv :: HasCallStack => Metadata -> LoggingConfig -> IO Katip.LogEnv
makeEnv metadata config =
  checkpointCallStack $ do
    let permitFunc = MinSeverity.toPermitFunc config.minSeverity
        colorStrategy = Color.toColorStrategy config.color

    env <-
      Katip.initLogEnv rootNamespace $
        Environment.toKatipEnvironment metadata.environment

    scribe <-
      Katip.mkHandleScribeWithFormatter
        (Format.toItemFormatter config.format)
        colorStrategy
        stdout
        permitFunc
        maxBound

    Katip.registerScribe "stdout" scribe Katip.defaultScribeSettings env


-- | Create 'LoggingData' that writes to 'stdout'. Uses a callback to ensure all
-- of the logs are flushed before exiting the program, even in the event of an
-- unhandled exception.
withStdout :: (HasCallStack, MonadUnliftIO m) => Metadata -> LoggingConfig -> (LoggingData -> m a) -> m a
withStdout metadata config use =
  checkpointCallStack $ withRunInIO $ \runInIO ->
    bracket (makeEnv metadata config) Katip.closeScribes $ \env ->
      runInIO . use $
        LoggingData
          { env
          , namespace = Katip.Namespace []
          , contexts = Katip.liftPayload $ Katip.sl "metadata" metadata
          }


-- | Run a 'Katip.KatipContextT' using 'LoggingData'.
runKatip :: LoggingData -> Katip.KatipContextT m a -> m a
runKatip loggingData =
  Katip.runKatipContextT loggingData.env loggingData.contexts loggingData.namespace


liftKatip :: (MonadReader r m, Has LoggingData r) => Katip.KatipContextT m a -> m a
liftKatip action = do
  loggingData <- asks getter
  runKatip loggingData action


liftKatipIO :: (MonadIO m, MonadReader r m, Has LoggingData r) => Katip.KatipContextT IO a -> m a
liftKatipIO action = do
  loggingData <- asks getter
  liftIO $ runKatip loggingData action
