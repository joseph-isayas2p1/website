module Main (main) where

import Discoslab.Webapi qualified as Webapi
import Discoslab.Webapi.Config qualified as Config
import GHC.Stack (HasCallStack)


main :: HasCallStack => IO ()
main = do
  config <- Config.fromEnv
  Webapi.start config
