module Discoslab.Webapi.TestApp
  ( stubbedData
  , TestApp (..)
  , with
  )
where

import Control.Monad.Cont (ContT (..))
import Control.Monad.IO.Class (MonadIO (..))
import Discoslab.Webapi.Admin.Emails qualified as AdminEmails
import Discoslab.Webapi.AppData (AppData (AppData))
import Discoslab.Webapi.AppData qualified as AppData
import Discoslab.Webapi.AppM qualified as AppM
import Discoslab.Webapi.Email.Address qualified as EmailAddress
import Discoslab.Webapi.Email.From (EmailFrom (EmailFrom))
import Discoslab.Webapi.Email.From qualified as EmailFrom
import Discoslab.Webapi.Email.Name qualified as EmailName
import Discoslab.Webapi.Http qualified as Http
import Discoslab.Webapi.Http.EnableSwagger (EnableSwagger (NoSwagger))
import Discoslab.Webapi.Logging.Data qualified as LoggingData
import Discoslab.Webapi.Metadata qualified as Metadata
import Discoslab.Webapi.Metadata.InstanceId qualified as InstanceId
import Discoslab.Webapi.Metadata.StartedAt qualified as StartedAt
import Discoslab.Webapi.Metadata.Version qualified as Version
import Discoslab.Webapi.Stubs.Email qualified as EmailStubs
import Network.Wai.Handler.Warp qualified as Warp


-- | The goal is to construct the most benign 'AppData' version as possible.
-- And then let the caller update the returned 'AppData' if they want more advanced stubs.
stubbedData :: IO AppData
stubbedData = do
  startedAt <- StartedAt.now
  instanceId <- InstanceId.nextRandom

  logging <- LoggingData.noLogs

  fromAddress <- do
    Right name <- pure $ EmailName.fromText "Test Suite"
    Right address <- pure $ EmailAddress.fromText "test.suite@example.com"
    pure EmailFrom{name = Just name, address}

  pure
    AppData
      { metadata = Metadata.def instanceId startedAt
      , logging
      , fromAddress
      , adminEmails = AdminEmails.def
      , sendEmail = EmailStubs.discardSentEmail
      }


-- | Information for interacting with our test application.
data TestApp = TestApp
  { httpPort :: Warp.Port
  , appData :: AppData
  }


-- | Start a new test application.
with :: AppData -> (TestApp -> IO a) -> IO a
with appData =
  runContT $ do
    waiApp <-
      liftIO . AppM.toIO appData $
        Http.makeWaiApplication Version.def NoSwagger

    httpPort <- ContT $ Warp.testWithApplication (pure waiApp)

    pure $ TestApp{..}
